package rmelian.challenge.weatherinfo;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Test;

/*
 * @author raisel.melian@gmail.com
 */
public class Tokenizer {

	@Test
	public void shouldConvertStringTokensToList() {
		String stringTokens = "33185,33186";
		assertEquals(Arrays.asList("33185", "33186"), StringTokenizerUtil.convertToList(stringTokens));
	}

	@Test
	public void shouldStringTokenDelimiterBeComma() {
		String stringTokens = "33185,33186 33186 33186 33186";
		assertEquals(2, StringTokenizerUtil.convertToList(stringTokens).size());
	}

	@Test
	public void shouldRemoveWhipeSpaces() {
		String stringTokens = "33185 , 33186   , 12345";
		assertEquals(Arrays.asList("33185", "33186", "12345"), StringTokenizerUtil.convertToList(stringTokens));
	}

}
