package rmelian.challenge.weatherinfo;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import rmelian.challenge.weatherinfo.cdyneapi.GetCityWeatherByZIPResponse;
import rmelian.challenge.weatherinfo.cdyneapi.WeatherReturn;
import rmelian.challenge.weatherinfo.service.WeatherClient;
import rmelian.challenge.weatherinfo.service.WeatherServiceImpl;

/*
 * @author raisel.melian@gmail.com
 */
@RunWith(MockitoJUnitRunner.class)
public class WeatherServiceTest {

	@Mock
	private WeatherClient weatherClient;

	private WeatherServiceImpl weatherService = new WeatherServiceImpl();  
	
	private final GetCityWeatherByZIPResponse response = new GetCityWeatherByZIPResponse();
	
	@Before
	public void setup() {
		weatherService.setWeatherClient(weatherClient);
		when(weatherClient.getGetCityWeatherByZIP(anyString())).thenReturn(response);
	}
	
	@Test
	public void shouldReturnOneWeatherReturn() {
		List<WeatherReturn> weatherReturnList = weatherService.getWeatherByZipcodes("33185");
		assertEquals(1, weatherReturnList.size());
	}
	
	@Test
	public void shouldReturnMultipleWeatherReturn() {
		List<WeatherReturn> weatherReturnList = weatherService.getWeatherByZipcodes("33185, 33190");
		assertEquals(2, weatherReturnList.size());
	}
}
