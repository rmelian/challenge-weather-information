var WeatherInfo = function() {
	
	return {

		initializeForm : function() {
			$( "#weather-form" ).submit(function( event ) {
				event.preventDefault();
				$("#weather-content-wrapper").empty();
				var form = $('#weather-form');
				form.validate({
					rules : {
						zipcodes : {
							required : true
						}
					},
					messages : {
						zipcodes : {
							required : 'You must type at least one zip code.'
						}
					},
					errorPlacement : function(error, element) {
						error.insertAfter(element);
					}
				});
				if (form.valid()) {
					$("#loader").toggleClass('fa-spinner');
					getWeatherInfo();
				}
			});
			
		}
	};
	
	function getWeatherInfo() {
		$.getJSON('/api/weatherinfo', {
			zipcodes : $('#zipcodes').val()
		}).done(function(response) {
			if (response.error) {
				renderUnexpectedError();
			} else {
				renderWeatherInfo(response);
			}
			$("#loader").toggleClass('fa-spinner');
		}).fail(function() {
			renderUnexpectedError();
			$("#loader").toggleClass('fa-spinner');
		});
	}

	function renderWeatherInfo(data) {
		$.each(data.weatherReturns, function(i, item) {
			$("#weather-content-wrapper").hide().append(renderWeatherContent(item)).fadeIn(500);
		});
	}
	
	function renderWeatherContent(item) {
		return '<div class="col-sm-4">' +
		'<div class="panel panel-default">' +
			'<div class="panel-heading">' +
				'<h3 class="panel-title">'+ item.city + ' ' + item.state + '</h3>' +
			'</div>' +
			'<div class="panel-body">' +
				'<p>Weather:'+ item.description + ' </p>' +  
				'<p>Pressure:'+ item.pressure + ' </p>' +
				'<p>Temperature:'+ item.temperature + ' </p>' +
				renderWSError(item) +
			'</div>' +
			'</div>' +
		'</div>';
	}
	
	function renderWSError(weatherItem) {
		if (!weatherItem.success) {
			return  '<div class="alert alert-danger">' + weatherItem.responseText + '</div>';
		}
		return '';
	}

	function renderUnexpectedError() {
		return  '<div class="alert alert-danger">Unexpected server error. Contact administrators</div>';
	}
	
}();