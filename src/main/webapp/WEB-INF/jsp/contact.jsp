<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description"
	content="Coding Challenge 1 – Weather Information">
<meta name="author" content="raisel melian">

<title>Coding Challenge 1 - Weather Information</title>

<link
	href="${pageContext.request.contextPath}/resources/vendor/font-awesome-4.0.3/css/font-awesome.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/vendor/bootstrap-3.3.1/css/bootstrap.min.css"
	rel="stylesheet">
<link
	href="${pageContext.request.contextPath}/resources/css/jumbotron-narrow.css"
	rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
	<div class="container">
		<div class="header">
			<nav>
				<ul class="nav nav-pills pull-right">
					<li role="presentation"><a href="/">Home</a></li>
					<li role="presentation" class="active"><a href="/contact">Contact</a></li>
				</ul>
			</nav>
			<h3 class="text-muted">Weather Information</h3>
		</div>
		<address>
		  <strong>Raisel Melian</strong><br>
		  <abbr title="Phone">P:</abbr> (786) 303-0014<br>
		  <a href="mailto:#">raisel.melian@gmail.com</a>
		</address>
		<br />
		<footer class="footer">
			<p>&copy; raisel melian 2015</p>
		</footer>
	</div>
</body>
</html>