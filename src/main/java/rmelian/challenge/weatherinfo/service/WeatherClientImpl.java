package rmelian.challenge.weatherinfo.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import rmelian.challenge.weatherinfo.cdyneapi.GetCityWeatherByZIP;
import rmelian.challenge.weatherinfo.cdyneapi.GetCityWeatherByZIPResponse;

/*
 * @author raisel.melian@gmail.com
 */
public class WeatherClientImpl extends WebServiceGatewaySupport implements WeatherClient {

	private static Logger logger = LoggerFactory.getLogger(WeatherClientImpl.class);

	@Value("${application.weatherws}")
	private String weatherws;

	@Override
	public GetCityWeatherByZIPResponse getGetCityWeatherByZIP(String zipCode) {
		GetCityWeatherByZIP request = new GetCityWeatherByZIP();
		request.setZIP(zipCode);
		logger.debug("Requesting weather for zipCode:" + zipCode);
		return (GetCityWeatherByZIPResponse) getWebServiceTemplate().marshalSendAndReceive(request, new SoapActionCallback(weatherws));
	}

}
