package rmelian.challenge.weatherinfo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import rmelian.challenge.weatherinfo.StringTokenizerUtil;
import rmelian.challenge.weatherinfo.cdyneapi.GetCityWeatherByZIPResponse;
import rmelian.challenge.weatherinfo.cdyneapi.WeatherReturn;

/*
 * @author raisel.melian@gmail.com
 */
@Component
public class WeatherServiceImpl implements WeatherService {

	@Autowired
	private WeatherClient weatherClient;

	@Override
	public List<WeatherReturn> getWeatherByZipcodes(String zipcodes) {
		List<WeatherReturn> weatherReturnList = new ArrayList<WeatherReturn>();
		for (String zipCode : StringTokenizerUtil.convertToList(zipcodes)) {
			weatherReturnList.add(getWeatherReturn(zipCode));
		}
		return weatherReturnList;
	}

	private WeatherReturn getWeatherReturn(String zipCode) {
		GetCityWeatherByZIPResponse response = weatherClient.getGetCityWeatherByZIP(zipCode);
		return response.getGetCityWeatherByZIPResult();
	}

	public WeatherClient getWeatherClient() {
		return weatherClient;
	}

	public void setWeatherClient(WeatherClient weatherClient) {
		this.weatherClient = weatherClient;
	}

}
