package rmelian.challenge.weatherinfo.service;

import rmelian.challenge.weatherinfo.cdyneapi.GetCityWeatherByZIPResponse;

/*
 * @author raisel.melian@gmail.com
 */
public interface WeatherClient {

	GetCityWeatherByZIPResponse getGetCityWeatherByZIP(String zipCode);

}
