package rmelian.challenge.weatherinfo.service;

import java.util.List;

import rmelian.challenge.weatherinfo.cdyneapi.WeatherReturn;

/*
 * @author raisel.melian@gmail.com
 */
public interface WeatherService {

	List<WeatherReturn> getWeatherByZipcodes(String zipcodes);

}
