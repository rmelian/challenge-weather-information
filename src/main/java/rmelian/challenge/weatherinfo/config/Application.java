package rmelian.challenge.weatherinfo.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
/*
 * @author raisel.melian@gmail.com
 */
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import rmelian.challenge.weatherinfo.service.WeatherClientImpl;

/*
 * @author raisel.melian@gmail.com
 */
@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = { "rmelian.challenge.weatherinfo" })
public class Application extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(Application.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setContextPath("rmelian.challenge.weatherinfo.cdyneapi");
		return marshaller;
	}

	@Bean
	public WeatherClientImpl weatherClient(Jaxb2Marshaller marshaller) {
		WeatherClientImpl client = new WeatherClientImpl();
		client.setDefaultUri("http://wsf.cdyne.com/WeatherWS/Weather.asmx");
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		return client;
	}

}
