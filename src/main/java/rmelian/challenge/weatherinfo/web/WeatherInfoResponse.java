package rmelian.challenge.weatherinfo.web;

import java.util.List;

import rmelian.challenge.weatherinfo.cdyneapi.WeatherReturn;

/*
 * @author raisel.melian@gmail.com
 */
class WeatherInfoResponse {

	private boolean error;

	List<WeatherReturn> weatherReturns;

	public List<WeatherReturn> getWeatherReturns() {
		return weatherReturns;
	}

	public WeatherInfoResponse(List<WeatherReturn> weatherReturns) {
		this.weatherReturns = weatherReturns;
	}

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

}
