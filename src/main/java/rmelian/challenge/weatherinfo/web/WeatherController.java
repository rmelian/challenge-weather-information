package rmelian.challenge.weatherinfo.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import rmelian.challenge.weatherinfo.cdyneapi.WeatherReturn;
import rmelian.challenge.weatherinfo.service.WeatherService;

/*
 * @author raisel.melian@gmail.com
 */
@RestController
@RequestMapping("/api/weatherinfo")
class WeatherController {

	private final WeatherService weatherService;

	@Autowired
	WeatherController(WeatherService weatherService) {
		this.weatherService = weatherService;
	}

	@RequestMapping(method = RequestMethod.GET)
	public WeatherInfoResponse getWeatherInfo(@RequestParam(value = "zipcodes") String zipcodes) {
		List<WeatherReturn> weatherinfo = weatherService.getWeatherByZipcodes(zipcodes);
		return new WeatherInfoResponse(weatherinfo);
	}

}
