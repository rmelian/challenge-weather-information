package rmelian.challenge.weatherinfo;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/*
 * @author raisel.melian@gmail.com
 */
public class StringTokenizerUtil {

	private static final String DELIMITER = ",";
	
	public static List<String> convertToList(String stringTokens) {
		StringTokenizer tokenizer = new StringTokenizer(stringTokens, DELIMITER);
		List<String> zipcodeList = new ArrayList<String>();
		while (tokenizer.hasMoreTokens()) {
			String zipcode = tokenizer.nextToken();
			zipcodeList.add(zipcode.trim());
		}
		return zipcodeList;
	}

	
}
